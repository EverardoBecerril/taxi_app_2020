import Pusher from 'pusher-js';

var pusher = new Pusher('3612647256c9472b619c', {
    cluster: 'us2',
    forceTLS: true
});

export default pusher;