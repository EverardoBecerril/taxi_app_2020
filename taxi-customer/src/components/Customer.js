import React, {useState, useEffect} from 'react';
import pusher from '../services/pusher_service';
import {Card, CardHeader, CardContent, TextField, Button} from '@material-ui/core';

function Customer(props) {
    const [pickupAddress, setPickupAddress] = useState('Paseo Destino');
    const [dropoffAddress, setDropoffAddress] = useState('Triángulo Las Ánimas');
    const [message, setMessage] = useState('');
    const [show, setShow] = useState(false);

    useEffect(() => {
        var channel = pusher.subscribe(`customer_${props.username}`);
        channel.bind('booking_status', (data) => {
            setMessage(data.msg);
            setShow(true);
        });
    }, [props]);

    const submitBookingRequest = () => {
        fetch("http://localhost:3000/api/bookings", {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                pickup_address: pickupAddress,
                dropoff_address: dropoffAddress,
                customer_id: props.username
            })
        }).then((response) => {
            return response.json();
        }).then((response) => {
            console.log(response);
        });
    }

    const mystlye = {
        minWidth: "50%",
        minHeight: 50
    };

    return (
        <div>
            {show ? <Card>
                <CardHeader title="Booking status"/>
                <CardContent>
                    {message}
                </CardContent>
            </Card> : null}
            <div>
                <TextField id="pickup-address" value={pickupAddress} onChange={(event) => setPickupAddress(event.target.value)} label="Pickup address" variant="outlined" style={mystlye} />
            </div>
            <div>
                <TextField id="dropoff-address" value={dropoffAddress} onChange={(event) => setDropoffAddress(event.target.value)} label="Drop-off address" variant="outlined" style={mystlye} />
            </div>
            <div>
                <Button id="submit-button" onClick={() => submitBookingRequest()} variant="outlined" size="large" color="primary" style={mystlye}>Submit request</Button>
            </div>
        </div>
    );
}

export default Customer;