Feature: Taxi booking
  As a customer
  Such that I go to destination
  I want to arrange a taxi ride

  Scenario: Booking a taxi ride through web page (with confirmation)
    Given the following taxis are on duty
      | driver_id | location    | status    |
      | juan      | Angelópolis | busy      |
      | pedro     | Angelópolis | available |
    And I want to go from "Atlixcáyotl 5718" to "Plaza Dorada"
    And I open the application's web page
    And I enter the booking information
    When I summit the booking request
    Then I should receive a confirmation message
    And "pedro" should receive a ride request
    When "pedro" accepts the ride request
    Then I should be notified "pedro" accepted the request