import React, {useState, useEffect} from 'react';
import pusher from '../services/pusher_service';
import {Card, CardHeader, CardContent, Button, CardActions} from '@material-ui/core';

function Driver(props) {
    const [pickupAddress, setPickupAddress] = useState('');
    const [dropoffAddress, setDropoffAddress] = useState('');
    const [show, setShow] = useState(false);

    useEffect(() => {
        var channel = pusher.subscribe(`driver_${props.username}`);
        channel.bind('booking_request', (data) => {
            setPickupAddress(data.pickup_address);
            setDropoffAddress(data.dropoff_address);
            setShow(true);
        });
    }, [props]);

    const acceptBookingRequest = () => {
        fetch("http://localhost:3000/api/bookings/123/accept", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
        }).then((response) => {
            setShow(false);
            console.log(response);
        });
    }

    return (
        <div>
            {
                show ?
                    <Card>
                        <CardHeader title="Booking request" />
                        <CardContent>
                            Trip from <b>{pickupAddress}</b> to <b>{dropoffAddress}</b>
                        </CardContent>
                        <CardActions>
                            <Button id="accept-button" onClick={() => acceptBookingRequest()} style={{ minWidth: "50%" }} variant="outlined" color="primary">Accept</Button>
                            <Button id="reject-button" style={{ minWidth: "50%" }} variant="outlined" color="secondary">Reject</Button>
                        </CardActions>
                    </Card>
                    : null
            }
        </div>
    );
}

export default Driver;