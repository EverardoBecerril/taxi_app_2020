import React from 'react';
import './App.css';

import Driver from './components/Driver';

function App(){
    return (
        <div className="App">
            <Driver username="pedro"/>
        </div>
    )
}

export default App;
