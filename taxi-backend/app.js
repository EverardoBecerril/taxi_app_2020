const express = require('express');
const logger = require('morgan');
const cors = require('cors');
 
const bookingsRouter = require('./routes/bookings');
 
const app = express();
app.use(cors());
 
app.use(logger('dev'));
app.use(express.json());
 
app.use('/api/bookings', bookingsRouter);
 
module.exports = app;