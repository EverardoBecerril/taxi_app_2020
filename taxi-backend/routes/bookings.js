const express = require('express');
const router = express.Router();
const pusher = require('../services/pusher_service');

router.post('/', (req, res) => {
    console.log(req.body);
    // System should:
    // - Find near-by taxis
    // - Select the one that is closest to the pickup address
    // - Contact such taxi, proposing him/her to take the ride
    // ----------------------------------
    // In this moment, the system always contacts "pedro"
    pusher.trigger('driver_pedro', 'booking_request', req.body);
    res.send({
        msg: 'We are processing your booking request'
    });
});

router.post('/:booking_id/accept', function(req, res, next) {
    pusher.trigger('customer_pepe', 'booking_status', {
        msg: 'Your taxi is on its way'
    });
    res.send();
});
  

module.exports = router;